r-cran-rjags (1:4-16-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 26 Sep 2024 08:50:09 +0900

r-cran-rjags (1:4-15-1) unstable; urgency=medium

  * Team upload.
  * Fix clean target
    Closes: #1046285

 -- Andreas Tille <tille@debian.org>  Fri, 22 Dec 2023 09:18:22 +0100

r-cran-rjags (1:4-14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 26 Jun 2023 20:18:08 +0200

r-cran-rjags (1:4-13-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4-13
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Tue, 17 May 2022 13:13:08 +0530

r-cran-rjags (1:4-12-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 17 Oct 2021 19:22:35 +0000

r-cran-rjags (1:4-11-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Add patch ac-path-pkgconfig.patch: Use cross-build compatible macro for
    finding pkg-config.

 -- Nilesh Patra <nilesh@debian.org>  Mon, 27 Sep 2021 16:19:48 +0530

r-cran-rjags (1:4-10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.4.1

 -- Andreas Tille <tille@debian.org>  Tue, 12 Nov 2019 14:11:26 +0100

r-cran-rjags (1:4-9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Mon, 02 Sep 2019 11:51:46 +0200

r-cran-rjags (1:4-8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 22 Oct 2018 11:21:33 +0200

r-cran-rjags (1:4-7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Fri, 12 Oct 2018 07:51:02 +0200

r-cran-rjags (1:4-6-3) unstable; urgency=medium

  * Team upload.
  * Secure URI in watch file
  * Take over package into r-pkg team
  * cme fix dpkg-control
  * debhelper 11
  * Convert from cdbs to dh-r
  * Canonical Homepage for CRAN packages
  * DEP5
  * Testsuite: autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Sun, 11 Mar 2018 20:55:31 +0100

r-cran-rjags (1:4-6-2) unstable; urgency=medium

  * Update standards-version; fix Lintian issues.

 -- Chris Lawrence <lawrencc@debian.org>  Sun, 01 Oct 2017 00:30:39 -0400

r-cran-rjags (1:4-6-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 01 Mar 2016 19:59:45 -0500

r-cran-rjags (1:4-5-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Mon, 11 Jan 2016 19:23:47 -0500

r-cran-rjags (1:4-4-1) unstable; urgency=medium

  * New upstream release.
  * Requires JAGS 4.0.0-1 or later.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 28 Oct 2015 13:52:04 -0400

r-cran-rjags (1:3-15-2) unstable; urgency=medium

  * Rebuild for g++ 5.2 transition.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 11 Aug 2015 21:18:33 -0400

r-cran-rjags (1:3-15-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 29 Apr 2015 19:09:52 -0400

r-cran-rjags (1:3-14-1) unstable; urgency=medium

  * New upstream release

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 13 Nov 2014 10:08:31 -0500

r-cran-rjags (1:3-13-2) unstable; urgency=medium

  * Fix Homepage URL.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 23 Apr 2014 21:42:07 -0400

r-cran-rjags (1:3-13-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Sun, 09 Mar 2014 22:00:18 -0400

r-cran-rjags (3.10-1) unstable; urgency=low

  * New upstream release.
  * Rebuild for R 3.0.0.
  * Update to latest Standards-Version.

 -- Chris Lawrence <lawrencc@debian.org>  Sun, 31 Mar 2013 19:57:13 -0400

r-cran-rjags (3.3-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Mon, 25 Jul 2011 05:18:55 -0500

r-cran-rjags (2.2.0-2-2) unstable; urgency=low

  * Add dependency on r-cran-lattice (not in upstream DESCRIPTION).

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 15 Jan 2011 13:56:02 -0600

r-cran-rjags (2.2.0-2-1) unstable; urgency=low

  * Initial Debian Release.  (Closes: #610160)

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 15 Jan 2011 12:56:04 -0600
